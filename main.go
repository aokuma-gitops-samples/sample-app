package main

import (
	_ "embed"
	"fmt"
	"log"
	"net/http"
)

const port = 8080

//go:embed public/index.html
var htmlBody []byte

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "text/html")
		w.Write(htmlBody)
	})

	log.Printf("starting server on port %d", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
