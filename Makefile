.PHONY: build image-build image-push
CONTAINER_IMAGE=registry.gitlab.com/aokuma-gitops-samples/sample-app
VERSION?=$(shell git describe --tags --match="v*" --abbrev=14 HEAD)

DOCKER_BUILD_OPTS=""
ifeq ($(shell uname),Darwin)
	DOCKER_BUILD_OPTS=--platform linux/amd64
endif

build:
	go build -o server main.go

image-build:
	docker build $(DOCKER_BUILD_OPTS) -t $(CONTAINER_IMAGE):$(VERSION) .

image-push:
	docker push $(CONTAINER_IMAGE):$(VERSION)
