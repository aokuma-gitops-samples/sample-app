FROM golang:1.19.3 as builder

WORKDIR /usr/local/gitops-sample/

COPY . .

RUN CGO_ENABLED=0 go build -o server main.go


FROM scratch

COPY --from=builder /usr/local/gitops-sample/server /bin/server

ENTRYPOINT ["/bin/server"]
